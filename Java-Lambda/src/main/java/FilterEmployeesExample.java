import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import static java.util.stream.Collectors.groupingBy;

public class FilterEmployeesExample {
    public static void main (String []args){
        List<Employee> employees = Arrays.asList(
                new Employee("ASE", "Eslam", "0109"),
                new Employee("ASE", "Mohamed", "0109"),
                new Employee("ASE", "Ahmed", "0109"),
                new Employee("SE", "Mahmoud", "0109"),
                new Employee("SE", "Nour", "0109"),
                new Employee("SE", "Amgad", "0109"),
                new Employee("Senior", "Kareem", "0109"),
                new Employee("Senior", "Abdo", "0109"),
                new Employee("Manager", "Boss", "0109")

        );
        Collections.shuffle(employees);

        Map<String, List<Employee>> groupedEmployees = employees.stream()
                .collect(groupingBy(Employee::getTitle));


 /************************* Without Exception and Exception Handling **************/
//        groupedEmployees.forEach((title,group)->{
//                System.out.println("Title: " + title + " Count: " + group.size());
//                group.stream().forEach((e) -> {
//                    System.out.println("Name: " + e.getName() + " Phone: " + e.getMobile());
//                });
//        });
//

/************************* With Exception and its handling **************/
        /***** Method Reference *****/
//    groupedEmployees.forEach(PrintWrapperLambda(FilterEmployeesExample::print));

        /***** inline Lambda ******/
        groupedEmployees.forEach(PrintWrapperLambda((title,group)->{
            if(group.size()>=2) {
                System.out.println("Title: " + title + " Count: " + group.size());
                group.stream().forEach((e) -> {
                    System.out.println("Name: " + e.getName() + " Phone: " + e.getMobile());
                });
            }
            else{
                throw new RuntimeException ("Group is less than 2");
            }
        }));

     }

//     private static void print (String title,List<Employee> group) {
//        if(group.size()>=2) {
//                System.out.println("Title: " + title + " Count: " + group.size());
//                group.stream().forEach((e) -> {
//                    System.out.println("Name: " + e.getName() + " Phone: " + e.getMobile());
//                });
//            }
//            else{
//                throw new RuntimeException ("Group is less than 2");
//            }
//     }
    private static BiConsumer<String, List<Employee>> PrintWrapperLambda(BiConsumer<String, List<Employee>> consumer) {
        return (title, group) -> {
            try {
                consumer.accept(title, group);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        };
    }

}
